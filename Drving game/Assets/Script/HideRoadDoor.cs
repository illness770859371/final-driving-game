﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideRoadDoor : MonoBehaviour
{
    void OnTriggerEnter()
    {
        gameObject.SetActive(false);
    }
}
