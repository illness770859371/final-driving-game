﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    public GameObject door;
    public bool doorIsOpening;

    void Update()
    {
        if(doorIsOpening == true)
        {
            door.transform.Translate (Vector3.up * Time.deltaTime * 10);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        doorIsOpening = true;
    }
}
